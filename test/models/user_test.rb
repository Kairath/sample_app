require 'test_helper'

class UserTest < ActiveSupport::TestCase 
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = User.new(name: "example name", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "    "
    assert_not @user.valid?, "name cant be blank you fuk"
  end
  
  test "email presence" do
    @user.email = "  "
    assert_not @user.valid?, "email cant be blank fatass"
  end
  
  test "name shouldnt be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?, "user name cant be that long pigface"
  end
  
  test "email shouldnt be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?, "too long email butfuk"
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address.downcase
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  test "email should reject invalid adresses" do
    invalid_adresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_adresses.each do |x|
      @user.email  = x
      assert_not @user.valid?, "#{x.inspect} should be invalid"
    end
  end
  
  test "email must be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?, "username already taken!TRY ANOTHER ONE COUNT!"
  end
  
  test "email should  be saved as lower-case" do
    mixed_case_email = "Foo@eXample.cOm"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "pasword presence nonblank" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password minimum length 5" do
    @user.password = @user.password_confirmation = "a" * 5 
    assert_not @user.valid?
  end
  
end
